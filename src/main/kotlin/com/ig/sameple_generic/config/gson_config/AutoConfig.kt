package com.ig.sameple_generic.config.gson_config

import org.springframework.boot.autoconfigure.AutoConfiguration
import org.springframework.context.annotation.ComponentScan

@AutoConfiguration
@ComponentScan
open class AutoConfig {
}