package com.ig.sameple_generic.config.gson_config

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.ig.sameple_generic.config.gson_config.AnnotationExclusionStrategy
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Primary
import org.springframework.stereotype.Component

@Component
class GsonConfiguration {
    @Primary
    @Bean
    fun getGson(): Gson {
        return Gson()
    }

    @Bean
    fun getPrettyGson(): Gson {
        return GsonBuilder().setPrettyPrinting().create()
    }

    @Bean
    fun getGsonExpose(): Gson {
        return GsonBuilder()
            .setExclusionStrategies(AnnotationExclusionStrategy())
            .create()
    }

}