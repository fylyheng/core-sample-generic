package com.ig.sameple_generic.config.gson_config

import com.google.gson.ExclusionStrategy
import com.google.gson.FieldAttributes


class AnnotationExclusionStrategy : ExclusionStrategy {
    override fun shouldSkipField(f: FieldAttributes): Boolean {
        return f.getAnnotation(Exclude::class.java) != null
    }

    override fun shouldSkipClass(clazz: Class<*>?): Boolean {
        return false
    }
}