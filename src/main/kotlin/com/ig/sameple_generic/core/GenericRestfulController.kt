package com.ig.sameple_generic.core


import com.ig.sameple_generic.core.exception.entityExecption.NotFoundException
import com.ig.sameple_generic.core.exception.generalException.NotAcceptableException
import com.ig.sameple_generic.core.response.JSONFormat
import com.ig.sameple_generic.core.response.ResponseDTO
import com.ig.sameple_generic.utilities.AppConstant
import com.ig.sameple_generic.utilities.UtilService
import jakarta.persistence.criteria.CriteriaBuilder
import jakarta.persistence.criteria.Predicate
import jakarta.persistence.criteria.Root
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import java.lang.reflect.ParameterizedType
import java.util.*

abstract class GenericRestfulController<T : BaseEntity>() : DefaultFilter() {

    @Autowired
    lateinit var JSONFormat: JSONFormat

    @Autowired
    protected val repo: BaseRepository<T>? = null

    @Autowired
    lateinit var utilService: UtilService


    private var isAllowCreate: Boolean? = true
    private var isAllowDelete: Boolean? = true
    private var isAllowUpdate: Boolean? = true
    private var isAllowMultiProcess: Boolean? = false
    private var resourceName: String? = null

    constructor(
        allowUpdate: Boolean? = true,
        allowDelete: Boolean? = true,
        allowMultiProcess: Boolean? = false,
    ) : this() {
        this.isAllowDelete = allowDelete
        this.isAllowUpdate = allowUpdate
        this.isAllowMultiProcess = allowMultiProcess
        this.resourceName = this.getGenericTypeClass()?.simpleName
    }


    /**
     * - Base_APIs.
     *
     * - base api are fix with endpoint.
     * - base api we not allow to override.
     */

    @PostMapping
    open fun create(@RequestBody entity: T): ResponseDTO {

        this.beforeSave(entity)
        val obj = save(entity)
        this.afterSaved(obj)
        return JSONFormat.respondID(obj)
    }


    @PutMapping("{id}")
    open fun update(@PathVariable(value = "id") id: Long, @RequestBody entity: T?): ResponseDTO {

        this.beforeUpdate(entity!!)

        val data = repo?.save(
            update(id, entity)
        )!!

        this.afterUpdated(data)

        return JSONFormat.respondID(data)
    }


    @DeleteMapping("{id}")
    open fun delete(@PathVariable(value = "id") id: Long): ResponseDTO {

        this.checkAllowModify()
        try {
            repo?.deleteById(id)
        } catch (ex: EmptyResultDataAccessException) {
            notFound(id)
        }

        return JSONFormat.respondObj(null, status = HttpStatus.OK)
    }


    @PutMapping( "delete/{id}/{status}")
    open fun softDelete(@PathVariable id: Long, @PathVariable status: Boolean): ResponseDTO {
        val obj = this.getById(id)!!
        obj.status = status
        this.save(obj)
        return JSONFormat.respondObj(obj)
    }

    @GetMapping("{id}")
    open fun get(@PathVariable(value = "id") id: Long): ResponseDTO {
        return JSONFormat.respondObj(getById(id))
    }


    @GetMapping(AppConstant.ALL_PATH)
    open fun all(@RequestParam allParams: MutableMap<String, String>): ResponseDTO {
        val data = allCriteria(allParams) { _, _, _ -> }

        return JSONFormat.respondList(data)

    }

    @GetMapping(AppConstant.LIST_PATH)
    open fun list(@RequestParam allParams: Map<String, String>): ResponseDTO {
        val data = listCriteria(allParams)
        return JSONFormat.respondPage(data)
    }



//================================================================================================

    /**
     * @Open_override_APIs.
     *
     */

    open fun save(entity: T): T {
        return repo?.save(entity)!!
    }
    open fun save(entity: T, customFields: ((targetOBJ: T) -> Unit)? = {}): T {
        customFields?.invoke(entity)
        return repo?.save(entity)!!
    }

    open fun afterSaved(entity: T) {}
    open fun afterUpdated(entity: T) {}


    @Throws(Exception::class)
    open fun beforeSave(entity: T) {
        //step1
        validate(entity)
        //step3
    }

    open fun beforeUpdate(entity: T) {
        validate(entity)
    }

    /**
     * Basic Update function using for update Entity base on ID
     * @param id
     */
    fun update(id: Long, entity: T): T {

        val obj = this.getById(id)!!
        utilService.bindProperties(entity, obj)
        return obj
    }


    /**
     *
     * Update function with include and exclude parameter using for update Entity base on ID
     * @param fieldsProtected = list of Entity fields that use for ignore to update those fields
     *
     */
    fun update(id: Long, entity: T, fieldsProtected: List<String>? = null): T {
        val obj = this.getById(id)!!
        utilService.bindProperties(entity, obj, include = null, exclude = fieldsProtected)

        return obj
    }


    /**
     *
     * Update function with fieldsProtected parameter and customFields Lambda block using for update Entity base on ID
     * @param id = is a unit ID of Entity using for find record in DB for update
     * @param fieldsProtected = list of Entity fields that use for ignore to update those fields
     * @param customFields = is a Lambda block for Open to Client to Customize logic with the existing targetObj
     *
     */
    fun update(
        id: Long,
        entity: T,
        fieldsProtected: List<String>? = null,
        customFields: (targetObj: T) -> Unit = {},
    ): T {
        val obj = this.getById(id)!!
        utilService.bindProperties(entity, obj, exclude = fieldsProtected)
        customFields(obj)
        return obj
    }

    fun updateStatus(id: Long, fieldName: String, customStatus: String): T {
        val obj = getById(id)!!
        utilService.setValueToField(obj, fieldName, customStatus)
        return obj
    }

    fun getById(id: Long): T? {
        return repo?.findById(id)?.orElseThrow { notFound(id) }
    }

    fun listByIds(listEntityID: List<Long>, status: Boolean): MutableList<T>? {
        return repo?.findAllByIdInAndStatus(listEntityID, status)
    }


    private fun allCriteria(
        allParams: MutableMap<String, String>,
        addOnFilters: (predicates: ArrayList<Predicate>, cb: CriteriaBuilder, root: Root<T>) -> Unit,
    ): MutableList<T>? {
        val params = DefaultFilter(allParams.toMutableMap())

        return repo?.findAll({ root, _, cb ->
            val predicates = ArrayList<Predicate>()

            addOnFilters(predicates, cb, root)

            cb.and(*predicates.toTypedArray())
        }, Sort.by(params.sortDirection, params.orderBy))
    }


    private fun listCriteria(allParams: Map<String, String>): Page<T>? {

        val params = super.applyDefaultPaging(allParams)

        return repo?.findAll({ root, _, cb ->

            val predicates = ArrayList<Predicate>()
            cb.and(*predicates.toTypedArray())
        }, PageRequest.of(params.page, params.size, Sort.by(params.sortDirection, params.orderBy)))
    }


//================================================================================================


    protected fun notFound(id: Long): NotFoundException {
        throw NotFoundException("$resourceName id $id doesn't exists")
    }

    open fun validate(entity: T?): Boolean {
        //check files type between income data with Model datatype
        return true
    }

    private fun checkAllowModify() {
        when (false) {
            isAllowDelete -> {
                throw NotAcceptableException("$resourceName : Not allow to delete!")
            }

            isAllowUpdate -> {
                throw NotAcceptableException("$resourceName : Not allow to update!")
            }

            else -> {}
        }
    }

    private fun checkAllowCreate() {
        if (isAllowCreate == false) {

            throw NotAcceptableException("$resourceName : Not allow to create!")
        }
    }

    private fun checkAllowMultiProcess() {

    }

    @SuppressWarnings("unchecked")
    private fun getGenericTypeClass(): Class<T>? {
        return try {
            val className: String = (javaClass.genericSuperclass as ParameterizedType).actualTypeArguments[0].typeName
            val clazz = Class.forName(className)
            clazz as Class<T>
        } catch (e: java.lang.Exception) {
            throw IllegalStateException("Class is not parametrized with generic type!!! Please use extends <> ")
        }
    }

}