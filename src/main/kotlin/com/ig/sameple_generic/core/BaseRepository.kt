package com.ig.sameple_generic.core

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.NoRepositoryBean


@NoRepositoryBean
interface BaseRepository <T: BaseEntity> : JpaRepository<T,Long>, JpaSpecificationExecutor<T> {
    fun findAllByIdInAndStatus(ids:List<Long>, status:Boolean) : MutableList<T>
    fun findAllByStatusIsTrue() : MutableList<T>
}
