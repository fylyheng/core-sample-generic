package com.ig.sameple_generic.core.extension

import kotlin.math.abs
import kotlin.math.log10


infix fun <T> Boolean.then(param: () -> T): T? = if (this) param() else null


fun Int.length() = when (this) {
    0 -> 1
    else -> log10(abs(toDouble())).toInt() + 1
}

fun isNullOrEmpty(s: String?): Boolean = s.isNullOrEmpty()


fun String?.isNotNullOrNotEmpty(): String? {
    return when {
        this == null -> null
        this.isEmpty() -> null
        else -> this
    }
}


infix fun String?.notNullAndEmpty(block: () -> Unit) {
    if (this == null && this!="") block()
}

val Boolean?.int get() = if (this != null && this) 1 else 0
