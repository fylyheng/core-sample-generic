package com.ig.sameple_generic.core.extension

import com.ig.sameple_generic.core.BaseEntity
import jakarta.persistence.criteria.CriteriaBuilder
import jakarta.persistence.criteria.Predicate
import jakarta.persistence.criteria.Root
import org.springframework.stereotype.Component
import java.text.SimpleDateFormat
import java.util.*

@Component
class BaseEntityFilter {
    fun <T : BaseEntity> terminalFilter(
        terminalIds: List<String>?,
        predicates: ArrayList<Predicate>,
        root: Root<T>
    ) {
        if (!terminalIds.isNullOrEmpty()) {
            predicates.add(root.get<String>("terminalId").`in`(terminalIds))
        }
    }

    fun <T : BaseEntity> dateFilter(
        terminalIds: List<String>?,
        dateStr: String,
        cb: CriteriaBuilder,
        predicates: ArrayList<Predicate>,
        root: Root<T>
    ) {

        val dateFormat = SimpleDateFormat("yyyy-MM-dd")

        val date = dateFormat.parse(dateStr)
        val startDate = Date()
        val endDate = Date()

        // Create a date range filter
        predicates.add(cb.between(root.get<Date>("logDate"), startDate, endDate))

    }
}