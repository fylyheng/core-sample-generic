package com.ig.sameple_generic.core

import jakarta.persistence.MappedSuperclass


@MappedSuperclass
open class BaseEntityRequestDTO : BaseEntity()