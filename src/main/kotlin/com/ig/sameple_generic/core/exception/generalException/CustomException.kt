package com.ig.sameple_generic.core.exception.generalException

import com.ig.sameple_generic.core.response.HttpCode


class CustomException(status: HttpCode, message: String) : RuntimeException(message) {
    var status: Int = status.value
    var error: String = status.reasonPhrase
}