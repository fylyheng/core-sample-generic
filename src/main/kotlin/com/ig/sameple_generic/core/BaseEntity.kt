package com.ig.sameple_generic.core

import com.fasterxml.jackson.annotation.JsonIgnore
import jakarta.annotation.PreDestroy
import jakarta.persistence.*
import org.springframework.data.annotation.*
import org.springframework.data.annotation.Version
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.util.*


@MappedSuperclass
@EntityListeners(AuditingEntityListener::class)
open class BaseEntity {

//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Basic(optional = false)
//    @Column(name = "id", nullable = false, columnDefinition = "BIGINT")
//    private val id: Long = 0

    @Column(name = "status", nullable = false,columnDefinition = "boolean default true")
    open var status : Boolean = true

    @JsonIgnore(false)
    @Column(name = "version")
    @Version
    open var version: Int ?= 0

    @CreatedDate
    @JsonIgnore
    @Column(name = "date_created")
    open var created: Date? = null

    @LastModifiedDate
    @JsonIgnore
    @Column(name = "last_updated")
    open var updated: Date? = null


    @CreatedBy
    @JsonIgnore
    @Column(name = "created_by_id")
    open var createById: Long? = null


    @LastModifiedBy
    @JsonIgnore
    @Column(name = "updated_by_id")
    open var updatedById: Long? = null

    @PreDestroy
    protected fun onDelete() {
        status = false
    }

    companion object {

    }
}
