package com.ig.sameple_generic.core.response

class TreeResponse <T> (
    var data: T? = null,
    var children: List<TreeResponse<T>>? = null
)