package com.ig.sameple_generic.core.response

import org.springframework.stereotype.Component
import java.util.*

@Component
class ResponseDTO(

    var code: Int?,
    var message: String?,
    var timestamp: Date?,
    var data: Any?=null,
    var dataList: Any?=null,
    var total: Long? = null,
    var error: Any? = null
) {

    constructor() : this(data=null, dataList=null,code=null, message= null, timestamp=null)
}