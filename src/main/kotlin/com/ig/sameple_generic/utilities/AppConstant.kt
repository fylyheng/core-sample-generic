package com.ig.sameple_generic.utilities

object AppConstant {

    /* APIs Uri */
    const val MAIN_PATH: String = "/v1/api"
    const val LIST_PATH: String = "/list"
    const val LIST_DTO_PATH: String = "/list-dto"
    const val ALL_PATH: String = "/all"
    const val UPDATE_SUBMIT_PATH: String = "/update-status-submit"
    const val UPDATE_CANCEL_PATH: String = "/update-status-cancel"
    const val UPDATE_STATUS_PATH: String = "/update-status"
    const val ALL_DROPDOWN: String = "/dropdown"
}
